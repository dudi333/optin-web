
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?php echo base_url()?>/assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>/assets/js/jquery-migrate.js"></script>
<script src="<?php echo base_url()?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>/assets/js/modernizr.min.js"></script>


    <link rel="shortcut icon" href="<?php echo base_url()?>/assets/img/logo-icon-telkomsel.png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina">
    <link rel="shortcut icon" href="javascript:;" type="<?php echo base_url()?>/assets/image/png">

    <title>Telkomsel Digital World</title>

    
    <!--bootstrap picker-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/js/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/js/bootstrap-timepicker/compiled/timepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/js/bootstrap-datetimepicker/css/datetimepicker.css"/>



    <!--Data Table-->
    <link href="<?php echo base_url()?>/assets/js/data-table/css/jquery.dataTables.css" rel="stylesheet">
    <link href="<?php echo base_url()?>/assets/js/data-table/css/dataTables.responsive.css" rel="stylesheet">

    <!--common style-->
    <link href="<?php echo base_url()?>/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/telkom.css">
    <link href="<?php echo base_url()?>/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="sticky-header"  ng-app="tdw">

    <section>
        <!-- sidebar left start-->
        <div class="sidebar-left">
            <!--responsive view logo start-->
            <div class="logo red-logo-bg padd0 text-center visible-xs-* visible-sm-*">
                <a href="tdw/dashboard">
                    <img src="<?php echo base_url()?>/assets/img/telkomsel-logo.png" alt="" width="180">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    
                </a>
            </div>
            <!--responsive view logo end-->

            <div class="sidebar-left-info">
                <!-- visible small devices start-->
                <div class=" search-field">  </div>
                <!-- visible small devices end-->

                <!--sidebar nav start-->
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Navigation</h3>
                    </li>

                    <li <?php if($this->uri->segment(2)=='dashboard' || $this->uri->segment(2)==null)echo "class='active'";?>><a href="<?php echo base_url();?>index.php/Tdw/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

                    <li <?php if($this->uri->segment(2)=='log')echo "class='active'";?>><a href="<?php echo base_url();?>index.php/Tdw/log"><i class="fa fa-bar-chart"></i> <span>Log</span></a></li>

                    <li <?php if($this->uri->segment(2)=='userlog')echo "class='active'";?>><a href="<?php echo base_url();?>index.php/Tdw/userlog"><i class="fa fa-users"></i> <span>User Log</span></a></li>
                    
                  
                </ul>
                <!--sidebar nav end-->


            </div>
        </div>
        <!-- sidebar left end-->

        <!-- body content start-->
        <div class="body-content">

            <!-- header section start-->
            <div class="header-section">

                <!--logo and logo icon start-->
                <div class="logo red-logo-bg hidden-xs hidden-sm padd0 text-center">
                    <a href="dashboard.html">
                        <img src="<?php echo base_url()?>/assets/img/telkomsel-logo.png" width="170">
                        <!-- <span class="brand-name"></span> -->
                    </a>
                </div>

                
                <!--logo and logo icon end-->

                <div class="icon-logo red-logo-bg hidden-xs hidden-sm">
                    <a href="index.html">
                        <img src="<?php echo base_url()?>/assets/img/logo-icon-telkomsel.png" alt="">
                        <!--<i class="fa fa-maxcdn"></i>-->
                    </a>
                </div>

                <!--toggle button start-->
                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                <!--toggle button end-->

               <div class="notification-wrap">
              
                <!--right notification start-->
                <div class="right-notification">
                    <ul class="notification-menu">


                        <li>
                            <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url()?>/assets/img/logo-icon-telkomsel.png" alt=""><?php echo $this->session->userdata('usrnametdw');?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                             <ul class="dropdown-menu dropdown-usermenu purple pull-right">                                
                                <li>
                                    <a href="<?php echo base_url()?>index.php/tdw/registration">
                                        <span class="label bg-info pull-right">new</span>Create Account
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url()?>/welcome/logout">
                                        <i class="fa fa-sign-out pull-right"></i> Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
              

                    </ul>
                </div>
                <!--right notification end-->
                </div>

            </div>
            <!-- header section end-->
             <!-- Content and header from here -->


             <?php
              echo "<pre>".json_encode($data_user,JSON_PRETTY_PRINT)."</pre>";
             ?>
            <br><br><br><br><br><br><br><br>
                <!--footer section start-->
            <footer>
                2016 &copy; All Rights Reserved
            </footer>
            <!--footer section end-->
        </div>
        <!-- body content end-->
    </section>
            


<!--bootstrap picker-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--picker initialization-->
<script src="<?php echo base_url()?>assets/js/picker-init.js"></script>
<!--Nice Scroll-->
<script src="<?php echo base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>

<!--Data Table-->
<script src="<?php echo base_url()?>assets/js/data-table/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/js/data-table/js/bootstrap-dataTable.js"></script>
<script src="<?php echo base_url()?>assets/js/data-table/js/dataTables.responsive.min.js"></script>
<!--data table init-->
<script src="<?php echo base_url()?>assets/js/data-table-init.js"></script>

<!--common scripts for all pages-->
<script src="<?php echo base_url()?>assets/js/scripts.js"></script>


</body>
</html>
