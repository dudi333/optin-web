<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Template">
    <meta name="keywords" content="admin dashboard, admin, flat, flat ui, ui kit, app, web app, responsive">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/ico/favicon.png">
    <title>Registration</title>

    <!-- Base Styles -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/telkom.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->


</head>

  <body class="login-body">
    <h3 align="center">Registration</h3>
      <div align="center">
      
                <p><a href="<?php echo $facebook;?>"><img style="width:325px" src="<?php echo base_url()?>assets/img/sign_in_fb.png"></a></p>
                <p><a href="<?php echo $twitter;?>"><img style="width:325px" src="<?php echo base_url()?>assets/img/sign_in_twit.png"></a></p>
                <p><a href="<?php echo $google;?>"><img style="width:325px" src="<?php echo base_url()?>assets/img/sign_in_google.jpg"></a></p>

                <form class="form-signin" action="<?php echo $form_action; ?>" method='post'>
                
                <?php $error_msg = $this->session->flashdata('error'); if(!empty($error_msg)){ ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Error!</strong> <?php  print_r($error_msg) ; ?>
                    </div>
                <?php } ?>

                <?php $ok_msg = $this->session->flashdata('ok'); if(!empty($ok_msg)){ ?>
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Success!</strong> <?php  echo $ok_msg; ?>
                    </div>
                <?php } ?>

                <?php $error_msg = $this->session->flashdata('error_otp'); if(!empty($error_msg)){ ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Error!</strong> <?php  print_r($error_msg) ; ?>
                    </div>
                <?php } ?>

                <?php $ok_msg = $this->session->flashdata('ok_otp'); if(!empty($ok_msg)){ ?>
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Success!</strong> <?php  echo $ok_msg; ?>
                    </div>
                <?php } ?>
                    <p class="label-p">Enter your personal details below</p>

                    <?php echo form_error('email'); ?>
                    <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
                    <?php echo form_error('password'); ?>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <input type="password" name="passwordconfirm" class="form-control" placeholder="Re-type Password"><br>

                    <?php echo form_error('msisdn'); ?>
                    <input type="text" name="msisdn" class="form-control" placeholder="Phone Number" autofocus>
                    <?php echo form_error('name'); ?>
                    <input type="text" name="name" class="form-control" placeholder="Full Name" autofocus>
                    <div class="check-box">
                        <label>
                            <input type="radio" name="gender" id="optionsRadios1" value="m" checked>
                            Male
                        </label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <label>
                            <input type="radio" name="gender" id="optionsRadios2" value="f">
                            Female
                        </label>
                    </div>
                  <?php echo form_error('birth_date'); ?>
	                <input type="text" name="birth_date" id="datepicker" class="form-control" placeholder="Date" />

                    <div>
	                    <label class="check-success">
	                        <input type="checkbox" value="agree this condition" id="checkbox1"> <label for="checkbox1">I agree to the Terms of Service and Privacy Policy</label>
	                    </label> 
                    </div>

                    <button class="btn btn-lg btn-default btn-block" name="send" type="submit" id="send"><?php echo $button; ?></button><br />
                    <p><a class="btn btn-lg btn-default btn-block" style="color:black" href="<?php echo site_url()?>/welcome/loginform">SIGN IN OPTIN MEMBER</a></p>
          		</form>

      </div>


      <!--jquery-1.10.2.min-->
      <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.min.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.js"></script>
      <!--Bootstrap Js-->
      <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jrespond..min.js"></script>

  </body>
</html>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      yearRange: '1945:2017',
      changeMonth: true,
      changeYear: true
    });
  });
</script>