<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Template">
    <meta name="keywords" content="admin dashboard, admin, flat, flat ui, ui kit, app, web app, responsive">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/ico/favicon.png">
    <title>Registration</title>

    <!-- Base Styles -->

    <link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/> 
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/telkom.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->


</head>

  <body class="login-body">
    <h3 align="center">Registration</h3>
      <div align="center">
                <?php if(isset($user_profile['picture'])) echo "<img src='".$user_profile['picture']."' width='250' height='250'><br /><br />" ?>
                <form class="form-signin" action="<?php echo $form_action; ?>" method='post'>
                    <span>Social ID :</span>
                    <input type="text" name="socmed_id" class="form-control" value="<?php echo $user_profile['id'];?>" readonly autofocus>
                    <span>Email :</span>
                    <input type="text" name="email" class="form-control" value="<?php if(isset($user_profile['email'])) echo $user_profile['email']; else echo ''; ?>" readonly autofocus>
                    <span>Full Name :</span>
                    <input type="text" name="name" class="form-control" value="<?php echo $user_profile['name'];?>" readonly autofocus>
                    <div class="check-box">
                        <label>
                            <input type="radio" name="gender" id="optionsRadios1" value="m" <?php if($user_profile['gender']=='m') echo 'checked'?> >
                            Male
                        </label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <label>
                            <input type="radio" name="gender" id="optionsRadios2" value="f" <?php if($user_profile['gender']=='f') echo 'checked'?>>
                            Female
                        </label>
                    </div>
                    <input type="text" name="birth_date" id="datepicker" class="form-control" placeholder="Choose your birth date..." />

                   
                    <?php echo form_error('msisdn'); ?>
                    <input type="text" name="msisdn" class="form-control" placeholder="Type your phone number..." autofocus>
                    <?php echo form_error('password'); ?>

                    <input type="password" name="password" class="form-control" placeholder="Type your password..." autofocus>
                    <input type="password" name="passwordconfirm" class="form-control" placeholder="Re-type Password...">
                    <div>
	                    <label class="check-success">
	                        <input type="checkbox" value="agree this condition" id="checkbox1"> <label for="checkbox1">I agree to the Terms of Service and Privacy Policy</label>
	                    </label> 
                    </div>

                    <button class="btn btn-lg btn-default btn-block" name="send" type="submit" id="send"><?php echo $button; ?></button><br><br>
          		</form>
          		<!-- <p><a href="<?php echo site_url('facebook_control/logout');?>">Sign Out</a></p> -->
      </div>


      <!--jquery-1.10.2.min-->
      <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.min.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.js"></script>
      <!--Bootstrap Js-->
      <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jrespond..min.js"></script>

      <script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>

  </body>
</html>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      yearRange: '1945:2017',
      changeMonth: true,
      changeYear: true
    });
  });
</script>