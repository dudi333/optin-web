<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Template">
    <meta name="keywords" content="admin dashboard, admin, flat, flat ui, ui kit, app, web app, responsive">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/ico/favicon.png">
    <title>Registration</title>

    <!-- Base Styles -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/telkom.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->


</head>

  <body class="login-body">
    <h2 align="center">Registration</h2>
      <div align="center">

                <form class="form-signin" action="<?php echo $form_action; ?>" method='post'>
                <!-- Notification From Middleware -->
                <?php $error_msg = $this->session->flashdata('error'); if(!empty($error_msg)){ ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Error!</strong> <?php  print_r($error_msg) ; ?>
                    </div>
                <?php } ?>

                <?php $ok_msg = $this->session->flashdata('ok'); if(!empty($ok_msg)){ ?>
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Success!</strong> <?php  echo $ok_msg; ?>
                    </div>
                <?php } ?>

                <?php $error_msg = $this->session->flashdata('error_otp'); if(!empty($error_msg)){ ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Error!</strong> <?php  print_r($error_msg) ; ?>
                    </div>
                <?php } ?>

                <?php $ok_msg = $this->session->flashdata('ok_otp'); if(!empty($ok_msg)){ ?>
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Success!</strong> <?php  echo $ok_msg; ?>
                    </div>
                <?php } ?>
                <!-- End of Notification -->
                    <p class="label-p"><h3><?php echo 'Welcome '.$user_profile['name'] ?></h3></p>
                    <!-- Input your personality Here -->
                    <img src="<?php echo $user_profile['picture'];?>"><br /><br />

                    <?php echo form_error('msisdn'); ?>
                    <input type="text" name="msisdn" class="form-control" placeholder="Insert Your Phone Number..." autofocus><br>
                    <?php echo form_error('otp'); ?>
                    <input type="text" name="otp" class="form-control" placeholder="Verify OTP code..." autofocus><br>
                    
                    <!-- End of input personality -->
                    <div>
	                    <label class="check-success">
	                        <input type="checkbox" value="agree this condition" id="checkbox1"> <label for="checkbox1">I agree to the Terms of Service and Privacy Policy</label>
	                    </label> 
                    </div>

                    <button class="btn btn-lg btn-default btn-block" name="send" type="submit" id="send"><?php echo $button; ?></button><br><br>
          		</form>
          		<!-- <p><a href="<?php echo site_url('google_control/logout');?>">Sign Out</a></p> -->
      </div>


      <!--jquery-1.10.2.min-->
      <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.min.js"></script>
  	  <script src="<?php echo base_url()?>assets/js/jquery-timepicker/jquery.timepicker.js"></script>
      <!--Bootstrap Js-->
      <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url()?>assets/js/jrespond..min.js"></script>

  </body>
</html>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datepicker1').datepicker({
      dateFormat: 'yy-mm-dd',
      yearRange: '1945:2017',
      changeMonth: true,
      changeYear: true
    });
  });
</script>