<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_control extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        $this->load->model('Regist_Model');
		$this->load->library('form_validation');

		$session = $this->session->userdata("session_api_id");
	}

	public function index()
	{
		/*
		if($this->session->userdata('login') == true){
			redirect('google_control/google_sign');
		}
		*/
		if (isset($_GET['code'])) {

			$this->googleplus->getAuthenticate();

			$user_profile = $this->googleplus->getUserInfo();

			$array = array();
			$array['id'] = $user_profile['id'];
			$array['email'] = (isset($user_profile['email'])) ? $user_profile['email'] : '';
			$array['name'] = $user_profile['name'];
			$array['picture'] = (isset($user_profile['picture'])) ? $user_profile['picture'] : '';
			$array['gender'] = (isset($user_profile['gender'])) ? substr($user_profile['gender'],0,1) : '';
			$array['birth_day'] = (isset($user_profile['birth_day'])) ? $user_profile['birth_day'] : '';

				
			$this->session->set_userdata('login',true);
			$this->session->set_userdata('user_profile',$array);
			
			/*$user = $this->googleplus->getUserInfo();

			$arr = array (
					'email' => $user['email'],
					'name' => $user['name'],
					'login_type' => 'google'
					);

			//echo var_dump($user);

				$add = $this->Regist_Model->insertSocmed($arr);

				if($add->error != TRUE)
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('ok', $add->message);
				}
				else
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('error', $add->message);				
				}*/

			redirect('Google_control/google_sign');
		}
	}
	
	public function google_sign()
	{
		
		if($this->session->userdata('login') != true){
			redirect('');
		}

		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('google_control/google_registrasi'),
			'msisdn' => $this->input->post('msisdn'),
			'user_profile' => $this->session->userdata('user_profile')
			);
		
		$this->load->view('google_sign.php',$contents);
		
	}

	public function google_registrasi()
	{
		$this->_rules();
		$user_profile = $this->session->userdata('user_profile');

		
		if ($this->form_validation->run() == FALSE) {
			$this->google_sign();

		} else {
			$data = array (
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password',TRUE),
				'msisdn' => $this->input->post('msisdn',TRUE),
				'name' => $this->input->post('name'),
				'gender' => substr($this->input->post('gender'),0,1),
				'birth_date' => $this->input->post('birth_date'),
				'login_type' => 'google',
				'socmed_id' => $this->input->post('socmed_id')
				);


			$add = $this->Regist_Model->insert($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok_msisdn', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error_msisdn', $add->message);				
			}

			redirect('google_control/verify_otp');
			//echo json_encode($add);
		}
	}

	public function verify_otp(){

		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('google_control/insert_otp'),
			'user_profile' => $this->session->userdata('user_profile')
			);

		$this->load->view('verify_otp',$contents);
		
	}

	public function insert_otp()
	{
		$this->OTP_rules();
		$user_profile = $this->session->userdata('user_profile');
		if ($this->form_validation->run() == FALSE) {
			$this->verify_otp();

		} else {
			$data = array (
				'otp' => $this->input->post('otp'),
				'msisdn' => $this->input->post('msisdn')
				);

			$add = $this->Regist_Model->otp_verify($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok_otp', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error_otp', $add->message);				
			}

			redirect('google_control/verify_otp');
			//echo json_encode($add);
		}

	}

	
    public function _rules()
    {
        $this->form_validation->set_rules('msisdn', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }

    public function OTP_rules()
    {
    	$this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required');
        $this->form_validation->set_rules('otp', 'OTP Code', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }
	
	public function logout(){
		
		$this->session->sess_destroy();
		$this->googleplus->revokeToken();
		redirect('');
		
	}
	
}
