<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_control extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        $this->load->model('Regist_Model');
		$this->load->library('form_validation');

		$session = $this->session->userdata("session_api_id");
	}

	public function index()
	{
		
		/*if($this->session->userdata('login') == true){
			redirect('facebook_control/fb_sign');
		}*/
		
		
		if ($this->facebook->logged_in())
		{
			$user = $this->facebook->user();

			if ($user['code'] === 200)
			{
				//var_dump($user['data']);exit;
				$this->session->set_userdata('login',true);
				$arr = array(
					'id'=>$user['data']['id'],
					'name' => $user['data']['name'],
					'email' =>$user['data']['email'],
					'screen_name' => $user['data']['screen_name'],
					'gender' => (isset($user['data']['gender'])) ? substr($user['data']['gender'],0,1) : '',
					'location' => $user['data']['location'],
					'description' => $user['data']['description'],
					'picture' => 'https://graph.facebook.com/'.$user['data']['id'].'/picture?type=large'
				);
				$this->session->set_userdata('user_profile',$arr);
				
				redirect('facebook_control/fb_sign');
				/*$arr = array (
					'email' => $user['data']['email'],
					'name' => $user['data']['name'],
					'login_type' => 'fb'
					);

				$add = $this->Regist_Model->insertSocmed($arr);

				if($add->error != TRUE)
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('ok', $add->message);
				}
				else
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('error', $add->message);				
				}*/
			}
		}
	}
	
	public function fb_sign(){
		/*if($this->session->userdata('login') != true){
			redirect('');
		}*/

		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('facebook_control/facebook_registrasi'),
			'user_profile' => $this->session->userdata('user_profile')
			);

		$this->load->view('facebook_sign.php',$contents);
		
	}

	public function verify_otp(){

		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('facebook_control/insert_otp'),
			'user_profile' => $this->session->userdata('user_profile')
			);

		$this->load->view('verify_otp',$contents);
		
	}
	
	public function insert_otp()
	{
		$this->OTP_rules();
		$user_profile = $this->session->userdata('user_profile');
		if ($this->form_validation->run() == FALSE) {
			$this->verify_otp();

		} else {
			$data = array (
				'otp' => $this->input->post('otp'),
				'msisdn' => $this->input->post('msisdn')
				);

			$add = $this->Regist_Model->otp_verify($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok_otp', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error_otp', $add->message);				
			}

			redirect('facebook_control/verify_otp');
			//echo json_encode($add);
		}

	}

	public function facebook_registrasi()
	{
		$this->_rules();
		$user_profile = $this->session->userdata('user_profile');
		if ($this->form_validation->run() == FALSE) {
			$this->fb_sign();

		} else {
			$data = array (
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password',TRUE),
				'msisdn' => $this->input->post('msisdn',TRUE),
				'name' => $this->input->post('name'),
				'gender' => $this->input->post('gender',TRUE),
				'birth_date' => $this->input->post('birth_date',TRUE),
				'login_type' => 'fb',
				'socmed_id' => $this->input->post('socmed_id')
				);

			$add = $this->Regist_Model->insert($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error', $add->message);				
			}

			redirect('facebook_control/verify_otp');
			//echo json_encode($add);
		}
	}

	public function _rules()
    {
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'trim|required');
        $this->form_validation->set_rules('msisdn', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }

    public function OTP_rules()
    {
    	$this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required');
        $this->form_validation->set_rules('otp', 'OTP Code', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('');
		
	}

	
}
