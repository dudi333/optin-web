<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter_control extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('Regist_Model');
        $this->load->library(array('twconnect','form_validation'));

		$session = $this->session->userdata("session_api_id");
	}
	public function index()
	{
		if($this->session->userdata('login') == true){
			redirect('twitter_control/profile');
		}
		
		$this->load->view('welcome_message');
	}
	
	public function redirect() {

		if($this->session->userdata('login') == true){
			redirect('twitter_control/profile');
		}
		
		$ok = $this->twconnect->twredirect('twitter_control/callback');

		if (!$ok) {
			echo 'Could not connect to Twitter. Refresh the page or try again later.';
		}
		
	}


	public function callback() {
		
		if($this->session->userdata('login') == true){
			redirect('twitter_control/profile');
		}
		
		$ok = $this->twconnect->twprocess_callback();
		
		if ( $ok ) { redirect('twitter_control/success'); }
			else redirect ('twitter_control/failure');
			
	}


	public function success() {

		/*if($this->session->userdata('login') == true){
			redirect('twitter_control/profile');
		}*/
		
		$this->twconnect->twaccount_verify_credentials();

		
		$user_profile = $this->twconnect->tw_user_info;
		$this->session->set_userdata('login',true);
		$arr = array(
			'id'=>$user_profile->id,
			'name' => $user_profile->name,
			'email' =>$user_profile->email,
			'screen_name' => $user_profile->screen_name,
			'gender' => (isset($user_profile->gender)) ? substr($user_profile->gender,0,1) : '',
			'location' => $user_profile->location,
			'description' => $user_profile->description,
			'picture' => $user_profile->profile_image_url
		);
		$this->session->set_userdata('user_profile',$arr);
		
		/*$data = array(
			'name' => $user_profile->name,
			'email' => null,
			'login_type' => 'twitter'
		);

		$add = $this->Regist_Model->insertSocmed($data);

		if($add->error != TRUE)
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('ok', $add->message);
				}
				else
				{
					//$this->session->unset_flashdata('ok');
					$this->session->set_flashdata('error', $add->message);				
				}
*/
		redirect('twitter_control/profile');
	}



	public function failure() {

		if($this->session->userdata('login') == true){
			redirect('twitter_control/profile');
		}
		
		echo '<p>Twitter connect failed</p>';
		echo '<p><a href="' . base_url() . 'twitter_control/logout">Try again!</a></p>';
	}
	
	public function profile(){
		if($this->session->userdata('login') != true){
			redirect('');
		}
		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('twitter_control/twitter_registrasi'),
			'msisdn' => $this->input->post('msisdn'),
			'user_profile' => $this->session->userdata('user_profile')
			);


		$this->load->view('twitter_sign',$contents);
		
	}

	public function twitter_registrasi()
	{
		$this->_rules();
		$user_profile = $this->session->userdata('user_profile');

		
		if ($this->form_validation->run() == FALSE) {
			$this->profile();

		} else {
			$data = array (
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password',TRUE),
				'msisdn' => $this->input->post('msisdn',TRUE),
				'name' => $this->input->post('name'),
				'gender' => substr($this->input->post('gender'),0,1),
				'birth_date' => $this->input->post('birth_date'),
				'login_type' => 'twitter',
				'socmed_id' => $this->input->post('socmed_id')
				);

			$add = $this->Regist_Model->insert($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error', $add->message);				
			}

			redirect('twitter_control/verify_otp');
			//echo json_encode($add);
		}
	}

	public function verify_otp(){

		$contents = array (
			'button' => 'Registrasi',
			'form_action' => site_url('twitter_control/insert_otp'),
			'user_profile' => $this->session->userdata('user_profile')
			);

		$this->load->view('verify_otp',$contents);
		
	}

	public function insert_otp()
	{
		$this->OTP_rules();
		$user_profile = $this->session->userdata('user_profile');
		if ($this->form_validation->run() == FALSE) {
			$this->verify_otp();

		} else {
			$data = array (
				'otp' => $this->input->post('otp'),
				'msisdn' => $this->input->post('msisdn')
				);

			$add = $this->Regist_Model->otp_verify($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok_otp', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error_otp', $add->message);				
			}

			redirect('twitter_control/verify_otp');
			//echo json_encode($add);
		}

	}

	public function OTP_rules()
    {
    	$this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required');
        $this->form_validation->set_rules('otp', 'OTP Code', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }

	public function _rules()
    {
        $this->form_validation->set_rules('msisdn', 'Phone Number', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('');
		
	}
}
