<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

        $this->load->model('Regist_Model');
        $this->load->model('Login_Model');
		$this->load->library('form_validation');

		$session = $this->session->userdata("session_api_id");
	}

	public function loginform(){
		$this->load->view('login');
	}

	public function login(){
		$data = array(
			'user_name'=>$this->input->post('username'),
			'password'=>$this->input->post('password')
			);
		$result = $this->Login_Model->login($data);
		if(!$result->error){
			$data_user = array(
				'session'=>$result->session,
				'message'=>$result->message,
				'id'=>$result->data->id,
				'user_name'=>$result->data->user_name,
				'email'=>$result->data->email,
				'name'=>$result->data->name,
				'msisdn'=>$result->data->msisdn,
				'login_type'=>$result->data->login_type,
				'group'=>$result->data->group,
				);
			$this->session->set_userdata('user_login_data',$data_user);
			redirect('welcome/home');
		}


	}

	public function home(){
			if(empty($this->session->userdata('user_login_data'))){
				redirect('welcome/loginform');
			}
			$data['data_user'] = $this->session->userdata('user_login_data');

			$this->load->view('home',$data);
	}

	public function index()
	{

		if($this->session->userdata('login') == true){
			$this->destroy_session_fb();
			$this->destroy_session_google();
		}

		$data = array (
			'button' => 'Sign Up',
			'form_action' => site_url('welcome/user_registrasi'),
			'email' => set_value('email'),
			'password' => set_value('password'),
			'msisdn' => set_value('msisdn'),
			'name' => set_value('name'),
			'gender' => set_value('gender'),
			'birth_date' => set_value('birth_date'),
			'facebook'=>$this->facebook->login_url(),
			'google'=>$this->googleplus->loginURL(),
			'twitter'=>site_url('twitter_control/redirect')
			);

		$this->load->view('welcome_message', $data);
	}

	public function user_registrasi()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$data = array(
				'email' => $this->input->post('email',TRUE),
				'password' => $this->input->post('password',TRUE),
				'msisdn' => $this->input->post('msisdn',TRUE),
				'name' => $this->input->post('name',TRUE),
				'gender' => $this->input->post('gender',TRUE),
				'birth_date' => $this->input->post('birth_date',TRUE),
				'login_type' => 'user'
				);
			$add = $this->Regist_Model->insert($data);

			if($add->error != TRUE)
			{
				$this->session->set_flashdata('ok', $add->message);
			}
			else
			{
				$this->session->set_flashdata('error', $add->message);				
			}

			redirect(site_url('welcome/otp_reg_manual'));
		}
	}

	public function otp_reg_manual(){

		$contents = array (
			'button' => 'Verify',
			'form_action' => site_url('welcome/insert_otp')
			);

		$this->load->view('verify_otp_regmanual',$contents);
		
	}

	public function insert_otp()
	{
		$this->OTP_rules();
		$user_profile = $this->session->userdata('user_profile');
		if ($this->form_validation->run() == FALSE) {
			$this->otp_reg_manual();

		} else {
			$data = array (
				'otp' => $this->input->post('otp'),
				'msisdn' => $this->input->post('msisdn')
				);

			$add = $this->Regist_Model->otp_verify($data);

			if($add->error != TRUE)
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('ok_otp', $add->message);
			}
			else
			{
				//$this->session->unset_flashdata('ok');
				$this->session->set_flashdata('error_otp', $add->message);				
			}

			redirect('welcome');
			//echo json_encode($add);
		}

	}


    public function _rules()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('msisdn', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('name', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('birth_date', 'Date', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }

    public function OTP_rules()
    {
    	$this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required');
        $this->form_validation->set_rules('otp', 'OTP Code', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger" style="color:white" >', '</span>');
    }

    public function destroy_session_google(){
		
		$this->session->sess_destroy();
		$this->googleplus->revokeToken();
		redirect('');
		
	}

	public function destroy_session_fb(){
		$this->session->sess_destroy();
		redirect('');
		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('welcome/loginform');
	}




}
