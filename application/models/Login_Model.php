<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_Model extends CI_Model
{

   function __construct()
    {
        parent::__construct();
    }

    function _request($url,$param,$type)
    {
        //call requesst API
        $config = array('server' => $this->config->item('rest_server'));

        $this->rest->initialize($config);

        if($type == "get")
        {
            $req = $this->rest->get($url,$param);
        }
        else
        {
            $req = $this->rest->post($url,$param);
        }

        return $req;

    }
    
    // insert data(array)
    function login($data)
    {
        $data['key'] = $this->config->item('rest_key');

        //call requesst API
        return $this->_request('user/login',$data,'post');
    }


    

}

/* End of file Member_model.php */
/* Location: ./application/models/Member_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-12-12 05:37:33 */
/* http://harviacode.com */
